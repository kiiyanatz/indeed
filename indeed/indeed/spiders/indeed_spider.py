from scrapy.spider import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from indeed.items import IndeedItem
from scrapy.utils.response import open_in_browser
import re


class IndeedSpider(CrawlSpider):
    name = "indeed"

    allowed_domains = ["indeed.com"]
    start_urls = [
        'https://www.indeed.com/jobs?q=Browse'
    ]
    rules = (
        Rule(
            LinkExtractor(
                allow=(),
                restrict_xpaths=(
                    '//div[@class="pagination"]/a/span/span[@class="np"]/parent::*/parent::*',
                )
            ), callback="parse_start_url", follow=True),)

    def parse_start_url(self, response):
        item = IndeedItem()
        for job in response.css("div.result"):
            item['job_title'] = ''.join(job.css('h2 a::text').extract())
            item['company_name'] = ''.join(
                job.css('span.company span a::text').extract())
            item['description'] = ''.join(
                job.css('table span.summary::text').extract())
            yield item
