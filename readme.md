# How to install

It requires Scrapy framework

Install the dependencies and devDependencies

```sh
$ sudo apt-get install python-pip
$ sudo pip install virtualenv
$ git clone https://kiiyanatz@bitbucket.org/kiiyanatz/indeed.git
$ cd indeed
$ virtualenv env
$ source env/bin/activate
$ pip install scrapy
$ cd indeed
```

Running the scraper

```sh
$ scrapy crawl indeed -o jobs.json
```
